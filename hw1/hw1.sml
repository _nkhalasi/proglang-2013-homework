fun make_date(year: int, month: int, day: int) = 
  if year < 1 orelse month < 1 orelse month > 12 orelse day < 1
  then NONE
  else 
    let 
      fun year_divisible_by(x: int) =
        (year mod x) = 0

      fun is_leap_year() = 
        year_divisible_by(400) orelse (year_divisible_by(4) andalso not(year_divisible_by(100)))

      fun is_february() =
        month = 2

      fun is_month_one_of_these(months: int list) = 
        if null months
        then false
        else if month = hd months
        then true
        else is_month_one_of_these(tl months)

      fun is_month_with_30_days() = 
        is_month_one_of_these([4,6,9,11])

      val days_in_feb = if is_leap_year() then 29 else 28
      val days_in_month = if is_february() then days_in_feb else if is_month_with_30_days() then 30 else 31
    in
      if day > days_in_month
      then NONE
      else SOME (year, month, day)
    end

fun reasonable_date(date: (int*int*int)) =
  if make_date(#1 date, #2 date, #3 date) = NONE
  then false
  else true

fun is_older(date1: (int*int*int), date2: (int*int*int)) =
  (#1 date1 < #1 date2) orelse (#2 date1 < #2 date2) orelse (#3 date1 < #3 date2)

fun number_in_month(dates: (int*int*int) list, month: int) =
  let
    fun count_dates_of_given_month(dates: (int*int*int) list, count: int) =
      if null dates
      then count
      else if #2 (hd dates) = month
      then count_dates_of_given_month(tl dates, count + 1)
      else count_dates_of_given_month(tl dates, count)
  in
    count_dates_of_given_month(dates, 0)
  end

fun number_in_months(dates: (int*int*int) list, months: int list) =
  let 
    fun count_dates_of_given_months(count: int, months: int list) =
      if null months
      then count
      else count_dates_of_given_months(count + number_in_month(dates, hd months), tl months)
  in
    count_dates_of_given_months(0, months)
  end

fun dates_in_month(dates: (int*int*int) list, month: int) =
  let
    fun filter(dates: (int*int*int) list, filtered_dates: (int*int*int) list) = 
      if null dates
      then rev filtered_dates
      else
        let
          val curr_date = hd dates
        in
          if (#2 curr_date) = month
          then filter(tl dates, curr_date :: filtered_dates)
          else filter(tl dates, filtered_dates)
        end
  in
    if null dates
    then []
    else filter(dates, [])
  end

fun dates_in_months(dates: (int*int*int) list, months: int list) =
  let
    fun filter(filtered_dates: (int*int*int) list, months: int list) =
      if null months
      then filtered_dates
      else filter(filtered_dates @ dates_in_month(dates, hd months), tl months)
  in
    if null dates
    then []
    else filter([], months)
  end

fun get_nth(strings: string list, pos: int) =
  let
    fun skip(strings: string list, curr_pos: int) =
      if null strings
      then ""
      else if curr_pos = pos
      then hd strings
      else skip(tl strings, curr_pos + 1)
  in
    if null strings orelse pos < 1
    then ""
    else skip(strings, 1)
  end

fun date_to_string(date: (int*int*int)) =
  let
    val months = ["January", "February", "March", "April", "May", "June", "July", 
                  "August", "September", "October", "November", "December"]
  in
    get_nth(months, #2 date) ^ " " ^ Int.toString(#3 date) ^ ", " ^ Int.toString(#1 date)
  end

fun number_before_reaching_sum(sum: int, nums: int list) = 
  let
    fun skip(nums: int list, running_sum: int, curr_index: int) = 
      if null nums
      then curr_index
      else
        let
          val new_sum = running_sum + (hd nums)
        in
          if new_sum >= sum
          then curr_index
          else skip(tl nums, new_sum, curr_index + 1)
        end
  in
    if null nums
    then 0
    else skip(nums, 0, 0)
  end

fun what_month(day_of_year: int) = 
  let
    val days = [0,31,28,31,30,31,30,31,31,30,31,30]
  in
    number_before_reaching_sum(day_of_year, days)
  end

fun month_range(day1: int, day2: int) =
  if day1 > day2
  then []
  else
    let
      fun build_month_range(range: int list, day: int) =
        if day > day2
        then rev range
        else build_month_range(what_month(day) :: range, day + 1)
    in
      build_month_range([], day1)
    end

fun oldest(dates: (int*int*int) list) =
  if null dates
  then NONE
  else
    let
      fun find_oldest(dates: (int*int*int) list, curr_oldest: (int*int*int)) =
        if null dates
        then SOME curr_oldest
        else if is_older(hd dates, curr_oldest)
        then find_oldest(tl dates, hd dates)
        else find_oldest(tl dates, curr_oldest)
    in
      find_oldest(tl dates, hd dates)
    end

fun remove_duplicates(nums: int list) = 
  let
    fun dedupe(nums: int list, curr_num: int, deduped_nums: int list) =
      if null nums
      then rev deduped_nums
      else
        let
          val next_num = hd nums
        in
          if next_num = curr_num
          then dedupe(tl nums, curr_num, deduped_nums)
          else dedupe(tl nums, next_num, next_num :: deduped_nums)
        end
  in
    dedupe(nums, ~1, [])
  end

fun number_in_months_challenge(dates: (int*int*int) list, months: int list) =
  number_in_months(dates, remove_duplicates(months))

fun dates_in_months_challenge(dates: (int*int*int) list, months: int list) =
  dates_in_months(dates, remove_duplicates(months))

