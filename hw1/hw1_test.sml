use "hw1.sml";

fun println(msg) =
  print("\n")

fun expect(expected, func, message) =
  if func() = expected
  then print("Ok..")
  else print("Failed: " ^ message ^ "..")

fun run_test_suite(name: string, test_suite_func) =
  let
  in
    println();
    print("Running " ^ name ^ "\n");
    test_suite_func();
    println()
  end

fun make_date_test_suite() = 
  let
  in
    expect(NONE, fn() => (make_date(0, 1, 1)), "Date has to be after January 1, 1");
    expect(NONE, fn() => (make_date(1900, 0, 1)), "Month cannot be less than 1");
    expect(NONE, fn() => (make_date(1900, 13, 1)), "Month cannot be more than 12");
    expect(NONE, fn() => (make_date(1900, 1, 32)), "January cannot have more than 31 days");
    expect(NONE, fn() => (make_date(1900, 4, 31)), "April cannot have more than 30 days");
    expect(NONE, fn() => (make_date(1900, 2, 30)), "February in a leap year cannot have more than 29 days");
    expect(NONE, fn() => (make_date(1900, 2, 29)), "February, 1900 does not have 29 days");
    expect(NONE, fn() => (make_date(1901, 2, 29)), "February in a non-leap year cannot have more than 29 days");
    expect(SOME(1900,2,28), fn() => (make_date(1900, 2, 28)), "February in a non-leap year has 28 days");
    expect(NONE, fn() => (make_date(3000, 2, 29)), "February, 3000 does not have 29 days");
    expect(SOME(2013,1,17), fn() => (make_date(2013, 1, 17)), "January 17, 2013");
    expect(SOME(2013,1,31), fn() => (make_date(2013, 1, 31)), "January 31, 2013");
    expect(SOME(1904,2,29), fn() => (make_date(1904, 2, 29)), "February 29, 1904")
  end

fun is_older_test_suite() =
  let
    val today = valOf(make_date(2013, 1, 17))
    val yesterday = valOf(make_date(2013, 1, 16))
  in
    expect(false, fn() => (is_older(today, yesterday)), "today cannot be earlier than yesterday");
    expect(true, fn() => (is_older(yesterday, today)), "yesterday should be earlier than today");
    expect(false, fn() => (is_older(today, today)), "today will never be earlier than today")
  end

fun number_in_month_test_suite() =
  let
  in
    expect(0, fn() => (number_in_month([], 1)), "Count should be zero for an empty list of dates");
    expect(0, fn() => (number_in_month([valOf(make_date(1901, 1, 1))], 12)), 
              "There is no date of December in the list");
    expect(1, fn() => (number_in_month([valOf(make_date(1901, 12, 1))], 12)), 
              "There is one date of December in the list");
    expect(3, fn() => (number_in_month([valOf(make_date(1901, 12, 1)), 
                                        valOf(make_date(1901, 11, 1)),
                                        valOf(make_date(1901, 11, 2)), 
                                        valOf(make_date(1902, 11, 12)),
                                        valOf(make_date(1902, 12, 1))], 11)), 
              "There are three dates of November in the list")
  end

fun number_in_months_test_suite() =
  let
  in
    expect(0, fn() => (number_in_months([], [1])), "Count should be zero for an empty list of dates");
    expect(0, fn() => (number_in_months([valOf(make_date(1901, 1, 1))], [12])), 
              "There is no date of December in the list");
    expect(1, fn() => (number_in_months([valOf(make_date(1901, 12, 1))], [12])), 
              "There is one date of December in the list");
    expect(1, fn() => (number_in_months([valOf(make_date(1901, 12, 1))], [1,12])), 
              "There is no date for January and one date of December in the list");
    expect(2, fn() => (number_in_months([valOf(make_date(1901, 12, 1)), 
                                         valOf(make_date(1901, 12, 14))], [1,12])), 
              "There is no date for January and two dates of December in the list");
    expect(5, fn() => (number_in_months([valOf(make_date(1901, 12, 1)), 
                                        valOf(make_date(1901, 11, 1)),
                                        valOf(make_date(1901, 11, 2)), 
                                        valOf(make_date(1902, 11, 12)),
                                        valOf(make_date(1902, 12, 1))], [11,12])), 
              "There are a total of 5 dates(three of November and two of December) in the list")
  end

fun number_in_months_challenge_test_suite() =
  let
  in
    expect(0, fn() => (number_in_months_challenge([], [1])), "Count should be zero for an empty list of dates");
    expect(0, fn() => (number_in_months_challenge([valOf(make_date(1901, 1, 1))], [12,12])), 
              "There is no date of December in the list");
    expect(1, fn() => (number_in_months_challenge([valOf(make_date(1901, 12, 1))], [12,12])), 
              "There is one date of December in the list");
    expect(1, fn() => (number_in_months_challenge([valOf(make_date(1901, 12, 1))], [1,1,12])), 
              "There is no date for January and one date of December in the list");
    expect(2, fn() => (number_in_months_challenge([valOf(make_date(1901, 12, 1)), 
                                         valOf(make_date(1901, 12, 14))], [1,12,12])), 
              "There is no date for January and two dates of December in the list");
    expect(5, fn() => (number_in_months_challenge([valOf(make_date(1901, 12, 1)), 
                                        valOf(make_date(1901, 11, 1)),
                                        valOf(make_date(1901, 11, 2)), 
                                        valOf(make_date(1902, 11, 12)),
                                        valOf(make_date(1902, 12, 1))], [11,11,11,12,12,12,12,12])), 
              "There are a total of 5 dates(three of November and two of December) in the list")
  end

fun dates_in_month_test_suite() =
  let
  in
    expect([], fn() => (dates_in_month([], 1)), "Should return an empty list for an empty list of dates");
    expect([], fn() => (dates_in_month([valOf(make_date(1901, 1, 1))], 12)), 
              "Should return an empty list since there is no date of December in the list");
    expect([(1901,12,1)], fn() => (dates_in_month([valOf(make_date(1901, 12, 1))], 12)), 
              "Should return a list with one date since there is one date of December in the list");
    expect([(1901,11,1), (1901,11,2), (1902,11,12)], 
           fn() => (dates_in_month([valOf(make_date(1901, 12, 1)), 
                                    valOf(make_date(1901, 11, 1)),
                                    valOf(make_date(1901, 11, 2)), 
                                    valOf(make_date(1912, 1, 1)),
                                    valOf(make_date(1902, 11, 12)),
                                    valOf(make_date(1902, 12, 1))], 11)), 
           "Should return a list with three dates since there are three dates of November in the list")
  end

fun dates_in_months_test_suite() =
  let
  in
    expect([], fn() => (dates_in_months([], [1])), "Should return an empty list for an empty list of dates");
    expect([], fn() => (dates_in_months([valOf(make_date(1901, 1, 1))], [12])), 
              "Should return an empty list since there is no date of December in the list");
    expect([(1901,12,1)], fn() => (dates_in_months([valOf(make_date(1901, 12, 1))], [12])), 
              "Should return a list with one date since there is one date of December in the list");
    expect([(1901,12,1)], fn() => (dates_in_months([valOf(make_date(1901, 12, 1))], [1,12])), 
              "Should return a list with one date since there is no date for January and one date of December in the list");
    expect([(1901, 12, 1), (1901, 12, 14)], 
           fn() => (dates_in_months([valOf(make_date(1901, 12, 1)), 
                                     valOf(make_date(1901, 12, 14))], [1,12])), 
              "Should return a list with two dates since there is no date for January and two dates of December in the list");
    expect([(1901,11,1), (1901,11,2), (1902,11,12), (1901,12,1), (1902,12,1)],
           fn() => (dates_in_months([valOf(make_date(1901, 12, 1)), 
                                     valOf(make_date(1901, 11, 1)),
                                     valOf(make_date(1901, 11, 2)), 
                                     valOf(make_date(1902, 11, 12)),
                                     valOf(make_date(1902, 12, 1))], [11,12])), 
              "Should return a list with 5 dates(three of November and two of December)")
  end

fun dates_in_months_challenge_test_suite() =
  let
  in
    expect([], fn() => (dates_in_months_challenge([], [1,1])), "Should return an empty list for an empty list of dates");
    expect([], fn() => (dates_in_months_challenge([valOf(make_date(1901, 1, 1))], [12,12])), 
              "Should return an empty list since there is no date of December in the list");
    expect([(1901,12,1)], fn() => (dates_in_months_challenge([valOf(make_date(1901, 12, 1))], [12,12,12,12])), 
              "Should return a list with one date since there is one date of December in the list");
    expect([(1901,12,1)], fn() => (dates_in_months_challenge([valOf(make_date(1901, 12, 1))], [1,1,1,1,12,12,12])), 
              "Should return a list with one date since there is no date for January and one date of December in the list");
    expect([(1901, 12, 1), (1901, 12, 14)], 
           fn() => (dates_in_months_challenge([valOf(make_date(1901, 12, 1)), 
                                     valOf(make_date(1901, 12, 14))], [1,1,1,1,12,12,12])), 
              "Should return a list with two dates since there is no date for January and two dates of December in the list");
    expect([(1901,11,1), (1901,11,2), (1902,11,12), (1901,12,1), (1902,12,1)],
           fn() => (dates_in_months_challenge([valOf(make_date(1901, 12, 1)), 
                                     valOf(make_date(1901, 11, 1)),
                                     valOf(make_date(1901, 11, 2)), 
                                     valOf(make_date(1902, 11, 12)),
                                     valOf(make_date(1902, 12, 1))], [11,11,11,11,11,12,12,12])), 
              "Should return a list with 5 dates(three of November and two of December)")
  end

fun get_nth_test_suite() =
  let
  in
    expect("", fn() => (get_nth([], 1)), "Should return empty string for an empty list");
    expect("", fn() => (get_nth(["one", "two", "three"], 0)), "Should return empty string for an invalid front end index");
    expect("", fn() => (get_nth(["one", "two", "three"], 4)), "Should return empty string for an invalid rear end index");
    expect("one", fn() => (get_nth(["one", "two", "three", "four"], 1)), "Should return the first element");
    expect("four", fn() => (get_nth(["one", "two", "three", "four"], 4)), "Should return the last element");
    expect("three", fn() => (get_nth(["one", "two", "three", "four"], 3)), "Should return the third element")
  end

fun date_to_string_test_suite() =
  let
  in
    expect("January 10, 1900", fn() => (date_to_string((1900, 1, 10))), "Should give you January 10, 1900");
    expect("December 25, 2000", fn() => (date_to_string((2000, 12, 25))), "Should give you December 25, 2000");
    expect("July 18, 1975", fn() => (date_to_string((1975, 7, 18))), "Should give you July 18, 1975")
  end

fun number_before_reaching_sum_test_suite() = 
  let
  in
    expect(0, fn() => (number_before_reaching_sum(10, [])), "Should give 0 for an empty list");
    expect(1, fn() => (number_before_reaching_sum(10, [1])), 
              "Should give 1 for an list with one element whose value is less than sum");
    expect(0, fn() => (number_before_reaching_sum(10, [10])), 
              "Should give 0 for an list with one element whose value is equal to sum");
    expect(0, fn() => (number_before_reaching_sum(1, [1,2])), 
              "Should give 0 for an list with two elements whose sum is more than sum");
    expect(1, fn() => (number_before_reaching_sum(2, [1,2])), 
              "Should give 1 for an list with two elements whose sum is less than sum");
    expect(1, fn() => (number_before_reaching_sum(3, [1,2])), 
              "Should give 1 for an list with two elements whose sum is equal to sum");
    expect(0, fn() => (number_before_reaching_sum(0, [1,2,3])), 
              "Should give 0 for an list with two elements - case 1");
    expect(0, fn() => (number_before_reaching_sum(1, [1,2,3])), 
              "Should give 0 for an list with two elements - case 2");
    expect(1, fn() => (number_before_reaching_sum(2, [1,2,3])), 
              "Should give 1 for an list with three elements - case 3 ");
    expect(1, fn() => (number_before_reaching_sum(3, [1,2,3])), 
              "Should give 1 for an list with three elements - case 3a ");
    expect(2, fn() => (number_before_reaching_sum(4, [1,2,3])), 
              "Should give 2 for an list with two elements - case 4");
    expect(2, fn() => (number_before_reaching_sum(5, [1,2,3])), 
              "Should give 2 for an list with two elements - case 5");
    expect(2, fn() => (number_before_reaching_sum(6, [1,2,3])), 
              "Should give 2 for an list with three elements - case 6");
    expect(3, fn() => (number_before_reaching_sum(7, [1,2,3])), 
              "Should give 3 for an list with three elements - case 7")
  end

fun what_month_test_suite() =
  let
  in
    expect(1, fn() => (what_month(1)), "Should give you 1 for 1st day of year");
    expect(1, fn() => (what_month(15)), "Should give you 1 for 15th day of year");
    expect(1, fn() => (what_month(31)), "Should give you 1 for 31st day of year");
    expect(2, fn() => (what_month(32)), "Should give you 2 for 32nd day of year");
    expect(2, fn() => (what_month(40)), "Should give you 2 for 40th day of year");
    expect(2, fn() => (what_month(59)), "Should give you 2 for 59th day of year");
    expect(3, fn() => (what_month(60)), "Should give you 3 for 60th day of year");
    expect(3, fn() => (what_month(75)), "Should give you 3 for 75th day of year");
    expect(3, fn() => (what_month(90)), "Should give you 3 for 90th day of year");
    expect(4, fn() => (what_month(91)), "Should give you 4 for 91st day of year");
    expect(4, fn() => (what_month(105)), "Should give you 4 for 105th day of year");
    expect(4, fn() => (what_month(120)), "Should give you 4 for 120th day of year");
    expect(5, fn() => (what_month(121)), "Should give you 5 for 121st day of year");
    expect(5, fn() => (what_month(135)), "Should give you 5 for 135th day of year");
    expect(5, fn() => (what_month(151)), "Should give you 5 for 151st day of year");
    expect(6, fn() => (what_month(152)), "Should give you 6 for 152nd day of year");
    expect(6, fn() => (what_month(165)), "Should give you 6 for 165th day of year");
    expect(6, fn() => (what_month(181)), "Should give you 6 for 181st day of year");
    expect(7, fn() => (what_month(182)), "Should give you 7 for 182nd day of year");
    expect(7, fn() => (what_month(195)), "Should give you 7 for 195th day of year");
    expect(7, fn() => (what_month(212)), "Should give you 7 for 212th day of year");
    expect(8, fn() => (what_month(213)), "Should give you 8 for 213th day of year");
    expect(8, fn() => (what_month(225)), "Should give you 8 for 225th day of year");
    expect(8, fn() => (what_month(243)), "Should give you 8 for 243rd day of year");
    expect(9, fn() => (what_month(244)), "Should give you 9 for 244th day of year");
    expect(9, fn() => (what_month(265)), "Should give you 9 for 265th day of year");
    expect(9, fn() => (what_month(273)), "Should give you 9 for 273rd day of year");
    expect(10, fn() => (what_month(274)), "Should give you 10 for 274th day of year");
    expect(10, fn() => (what_month(285)), "Should give you 10 for 285th day of year");
    expect(10, fn() => (what_month(304)), "Should give you 10 for 304th day of year");
    expect(11, fn() => (what_month(305)), "Should give you 11 for 305th day of year");
    expect(11, fn() => (what_month(315)), "Should give you 11 for 315th day of year");
    expect(11, fn() => (what_month(334)), "Should give you 11 for 334th day of year");
    expect(12, fn() => (what_month(335)), "Should give you 12 for 335th day of year");
    expect(12, fn() => (what_month(347)), "Should give you 12 for 347th day of year");
    expect(12, fn() => (what_month(365)), "Should give you 12 for 365th day of year")
  end

fun month_range_test_suite() =
  let
  in
    expect([], fn() => (month_range(2,1)), "Should return an empty list when start day > end day");
    expect([1], fn() => (month_range(1,1)), "Should return [1] since start day = end day = 1");
    expect([1,1], fn() => (month_range(1,2)), "Should return [1,1] since both days are in the same month");
    expect([1,2], fn() => (month_range(31,32)), "Should return [1,2] since start day is in Jan and end day is in Feb");
    expect([1,1,2,2], fn() => (month_range(30,33)), 
           "Should return [1,1,2,2] since the range has 2 days in Jan and 2 days in Feb")
  end

fun oldest_test_suite() =
  let
  in
    expect(NONE, fn() => (oldest([])), "Should return NONE for an empty list");
    expect(SOME (1901,1,1), fn() => (oldest [(1901,1,1)]), "Should return the same date as in the single element list");
    expect(SOME (1901,1,1), fn() => (oldest [(1901,1,1), (1901,1,2)]), "Should return (1901,1,1) - case 1");
    expect(SOME (1901,1,1), fn() => (oldest [(1901,1,10), (1901,1,5), (1901,1,1), (1901,2,20), (1901,2,5)]), "Should return (1901,1,1) - case 2")
  end

fun reasonable_date_test_suite() = 
  let
  in
    expect(false, fn() => (reasonable_date (0,1,1)), "0 is not a valid year");
    expect(true, fn() => (reasonable_date (1,1,1)), "January 1, 1")
  end

fun run_tests() = 
  let
  in
    run_test_suite("make_date_test_suite()", fn() => (make_date_test_suite()));
    run_test_suite("is_older_test_suite()", fn() => (is_older_test_suite()));
    run_test_suite("number_in_month_test_suite()", fn() => (number_in_month_test_suite()));
    run_test_suite("number_in_months_test_suite()", fn() => (number_in_months_test_suite()));
    run_test_suite("dates_in_month_test_suite()", fn() => (dates_in_month_test_suite()));
    run_test_suite("dates_in_months_test_suite()", fn() => (dates_in_months_test_suite()));
    run_test_suite("get_nth_test_suite()", fn() => (get_nth_test_suite()));
    run_test_suite("date_to_string_test_suite()", fn() => (date_to_string_test_suite()));
    run_test_suite("number_before_reaching_sum_test_suite()", fn() => (number_before_reaching_sum_test_suite()));
    run_test_suite("what_month_test_suite()", fn() => (what_month_test_suite()));
    run_test_suite("month_range_test_suite()", fn() => (month_range_test_suite()));
    run_test_suite("oldest_test_suite()", fn() => (oldest_test_suite()));
    run_test_suite("reasonable_date_test_suite()", fn() => (reasonable_date_test_suite()));
    run_test_suite("number_in_months_challenge_test_suite()", fn() => (number_in_months_challenge_test_suite()));
    run_test_suite("dates_in_months_challenge_test_suite()", fn() => (dates_in_months_challenge_test_suite()))
  end;

run_tests()

