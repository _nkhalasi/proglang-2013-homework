fun println(msg) =
  print("\n")

fun expect(expected, func, message) =
  if func() = expected
  then print("Ok..")
  else print("Failed: " ^ message ^ "..")

fun run_test_suite(name: string, test_suite_func) =
  let
  in
    println();
    print("Running " ^ name ^ "\n");
    test_suite_func();
    println()
  end

