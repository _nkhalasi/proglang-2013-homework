use "hw3.sml";

fun println() = print("\n")

fun expect(expected, func, message) =
    fn() => 
      if func() = expected
      then print("Ok..")
      else print("Failed: " ^ message ^ "..")

fun run_test_suite test_suite =
  case test_suite of
       [] => ()
     | test::tests => (test(); run_test_suite tests);

fun run_tests(name, test_suite) =
  let
  in
    println();
    print("Running " ^ name ^ "\n");
    run_test_suite test_suite;
    println()
  end;

run_tests("only_capitals() test suite", [
    expect([], fn() => only_capitals [], "Should return empty list for an empty list"),
    expect([], fn() => only_capitals ["naresh"], "Should return empty list for list not containing valid values - case 1"),
    expect([], fn() => only_capitals ["naresh", "khalasi"], "Should return empty list for list not containing valid values - case 2"),
    expect(["Naresh"], fn() => only_capitals ["Naresh"], "Should return list with valid items for list containing valid and invalid values - case 1"),
    expect(["Naresh"], fn() => only_capitals ["Naresh", "khalasi"], "Should return list with valid items for list containing valid and invalid values - case 2"),
    expect(["Naresh", "Khalasi"], fn() => only_capitals ["Naresh", "Khalasi"], "Should return list with valid items for list containing valid and invalid values - case 3")
  ]);

run_tests("longest_string1() test suite", [
    expect("", fn() => longest_string1 [], "Should return \"\" for an empty list"),
    expect("naresh", fn() => longest_string1 ["naresh"], "Should return the only element in the list"),
    expect("naresh", fn() => longest_string1 ["naresh", "ramesh"], "Should return the first element in the list containing elements of same size"),
    expect("krishang", fn() => longest_string1 ["naresh", "krishang", "ramesh"], "Should return the longest string - case 1"),
    expect("aaaaa", fn() => longest_string1 ["bbb", "aa", "aaaaa", "dddd", "fffff", "b"], "Should return the longest string - case 2")
  ]);

run_tests("longest_string2() test suite", [
    expect("", fn() => longest_string2 [], "Should return \"\" for an empty list"),
    expect("naresh", fn() => longest_string2 ["naresh"], "Should return the only element in the list"),
    expect("ramesh", fn() => longest_string2 ["naresh", "ramesh"], "Should return the last element in the list containing elements of same size"),
    expect("krishang", fn() => longest_string2 ["naresh", "krishang", "ramesh"], "Should return the longest string - case 1"),
    expect("fffff", fn() => longest_string2 ["bbb", "aa", "aaaaa", "dddd", "fffff", "b"], "Should return the longest string - case 2")
  ]);

run_tests("longest_string3() test suite", [
    expect("", fn() => longest_string3 [], "Should return \"\" for an empty list"),
    expect("naresh", fn() => longest_string3 ["naresh"], "Should return the only element in the list"),
    expect("naresh", fn() => longest_string3 ["naresh", "ramesh"], "Should return the first element in the list containing elements of same size"),
    expect("krishang", fn() => longest_string3 ["naresh", "krishang", "ramesh"], "Should return the longest string - case 1"),
    expect("aaaaa", fn() => longest_string3 ["bbb", "aa", "aaaaa", "dddd", "fffff", "b"], "Should return the longest string - case 2")
  ]);

run_tests("longest_string4() test suite", [
    expect("", fn() => longest_string4 [], "Should return \"\" for an empty list"),
    expect("naresh", fn() => longest_string4 ["naresh"], "Should return the only element in the list"),
    expect("ramesh", fn() => longest_string4 ["naresh", "ramesh"], "Should return the last element in the list containing elements of same size"),
    expect("krishang", fn() => longest_string4 ["naresh", "krishang", "ramesh"], "Should return the longest string - case 1"),
    expect("fffff", fn() => longest_string4 ["bbb", "aa", "aaaaa", "dddd", "fffff", "b"], "Should return the longest string - case 2")
  ]);

run_tests("longest_capitalized() test suite", [
    expect("", fn() => longest_capitalized [], "Should return \"\" for an empty list"),
    expect("", fn() => longest_capitalized ["naresh"], "Should return the only element in the list"),
    expect("", fn() => longest_capitalized ["naresh", "ramesh"], "Should return the first element in the list containing elements of same size"),
    expect("Krishang", fn() => longest_capitalized ["naresh", "Krishang", "ramesh"], "Should return the longest string - case 1"),
    expect("Aaaaa", fn() => longest_capitalized ["Bbb", "Aa", "Aaaaa", "Dddd", "Fffff", "B"], "Should return the longest string - case 2")
  ]);

run_tests("rev_string() test suite", [
    expect("", fn() => rev_string "", "Should return \"\" for an empty string"),
    expect("hseraN", fn() => rev_string "Naresh", "Should return hseraN for Naresh"),
    expect("A", fn() => rev_string "A", "Should return A for A")
  ]);

fun answer_helper x = if x = "answer" then SOME x else NONE;
run_tests("first_answer() test suite", [
    expect("NONE", fn() => (first_answer answer_helper []) handle NoAnswer => "NONE", "Should raise an exception for an empty list"),
    expect("NONE", fn() => (first_answer answer_helper ["abc"]) handle NoAnswer => "NONE", "Should raise an exception for list not containing the answer"),
    expect("answer", fn() => (first_answer answer_helper ["abc", "def", "answer"]) handle NoAnswer => "NONE", "Should return answer for list containing the answer")
  ]);

fun answer_helper2 x = if is_capitalized x then SOME [x] else NONE;
val all_answers_c = all_answers answer_helper2;
run_tests("all_answers() test suite", [
    expect(SOME [], fn() => all_answers_c [], "Should return SOME [] for an empty list"),
    expect(NONE, fn() => all_answers_c ["abc"], "Should return NONE for list not containing the answer"),
    expect(NONE, fn() => all_answers_c ["Naresh", "Krishang", "abc"], "Should return NONE for list containing atleast one invalid answer"),
    expect(SOME ["Khalasi", "Krishang", "Naresh"], fn() => all_answers_c ["Naresh", "Krishang", "Khalasi"], "Should return a list containing all elements")
  ]);

run_tests("count_wildcards() test suite", [
    expect(0, fn() => count_wildcards (TupleP []), "Should return 0 when there are no wildcard patterns - case 1"),
    expect(0, fn() => count_wildcards (TupleP [UnitP, ConstP 10]), "Should return 0 when there are no wildcard patterns - case 2"),
    expect(1, fn() => count_wildcards (TupleP [Wildcard]), "Should return 1"),
    expect(2, fn() => count_wildcards (TupleP [Wildcard, Wildcard, UnitP]), "Should return 2"),
    expect(3, fn() => count_wildcards (TupleP [Wildcard, Wildcard, UnitP, Wildcard]), "Should return 3"),
    expect(4, fn() => count_wildcards (TupleP [Wildcard, Wildcard, UnitP, Wildcard, ConstP 10, Wildcard]), "Should return 4")
  ]);

run_tests("count_wild_and_variable_lengths() test suite", [
    expect(0, fn() => count_wild_and_variable_lengths (TupleP []), "Should return 0 when there are no wildcard and variable patterns - case 1"),
    expect(0, fn() => count_wild_and_variable_lengths (TupleP [UnitP, ConstP 10]), "Should return 0 when there are no wildcard and variable patterns - case 2"),
    expect(1, fn() => count_wild_and_variable_lengths (TupleP [Wildcard]), "Should return 1"),
    expect(4, fn() => count_wild_and_variable_lengths (TupleP [Wildcard, Variable "abc"]), "Should return 4"),
    expect(5, fn() => count_wild_and_variable_lengths (TupleP [Wildcard, Wildcard, UnitP, Variable "abc"]), "Should return 5"),
    expect(8, fn() => count_wild_and_variable_lengths (TupleP [Wildcard, Wildcard, UnitP, Variable "abc", Variable "def"]), "Should return 8")
  ]);

run_tests("count_some_var() test suite", [
    expect(0, fn() => count_some_var("abc", UnitP), "Should return 0 for a non-variable pattern"),
    expect(0, fn() => count_some_var("abc", Variable "def"), 
          "Should return 0 for a Variable with different name"),
    expect(0, fn() => count_some_var("abc", TupleP [UnitP]), 
          "Should return 0 for a TupleP pattern not containing Variable"),
    expect(0, fn() => count_some_var("abc", TupleP [Variable "def"]), 
          "Should return 0 for a TupleP pattern containing Variable but different name"),
    expect(1, fn() => count_some_var("abc", Variable "abc"), "Success case 1"),
    expect(1, fn() => count_some_var("abc", TupleP [Variable "abc"]), "Success case 2"),
    expect(2, fn() => count_some_var("abc", TupleP [Variable "abc", Variable "def", Variable "abc", UnitP]), "Success case 3")
  ]);

run_tests("check_pat() test suite", [
    expect(true, fn() => check_pat (TupleP []), "Should return true for empty pattern list"),
    expect(true, fn() => check_pat (TupleP [UnitP, ConstP 10]), 
          "Should return true for a pattern list not containing variables"),
    expect(true, fn() => check_pat (TupleP [UnitP, Variable "abc"]), 
          "Should return true for a pattern list containing one variable"),
    expect(true, fn() => check_pat (TupleP [Variable "def", UnitP, Variable "abc"]), 
          "Should return true for a pattern list containing two variables"),
    expect(false, fn() => check_pat (TupleP [Variable "def", Variable "abc", UnitP, Variable "abc"]), 
          "Should return true for a pattern list containing duplicate variables"),
    expect(false, fn() => check_pat (TupleP [TupleP [Variable "x", ConstructorP("wild", Wildcard)], Variable "x"]), 
          "Should return false for duplicate patterns nested inside tuples"),
    expect(false, fn() => check_pat (TupleP [TupleP [TupleP [Variable "x",ConstructorP("wild", Wildcard)], Wildcard], Variable "x"]),
          "Should return false for duplicate patterns deeply nested inside tuples")
  ]);

run_tests("match() test suite", [
    expect(NONE, fn() => match(Unit, ConstP 1), "Should return NONE - case 1"),
    expect(NONE, fn() => match(Const 1, ConstructorP("abc", ConstP 1)), "Should return NONE - case 2"),
    expect(SOME [], fn() => match(Unit, UnitP), "Should return SOME [] - case 1"),
    expect(SOME [], fn() => match(Const 1, ConstP 1), "Should return SOME [] - case 2"),
    expect(SOME [], fn() => match(Tuple [Const 1, Unit], Wildcard), "Should return SOME [] - case 3"),
    expect(SOME [("a", Const 1)], fn() => match(Constructor("a", Const 1), ConstructorP("a", Variable "a")), "Should return SOME [(\"a\", Const 1)]"),
    expect(SOME [], fn() => match(Constructor("a", Const 1), ConstructorP("a", Wildcard)), "Should return SOME [] - case 4"),
    expect(SOME [], fn() => match(Tuple [], TupleP []), "Should return SOME [] - case 5"),
    expect(SOME [("b", Const 5), ("a", Unit)], fn() => match(Tuple [Unit, Const 3, Const 5], TupleP [Variable "a", Wildcard, Variable "b"]), 
          "Should return SOME [(\"a\", Unit), (\"b\", Const 5)]"),
    expect(NONE, fn() => match(Tuple [Const 17, Unit, Const 4, Constructor("egg", Const 4),Constructor("egg", Constructor("egg", Const 4))], TupleP [Wildcard,Wildcard]),
          "Should return NONE for tuples with different number of values and patterns"),
    expect(NONE, fn() => match(Const 10, ConstP 14), "Should return NONE for different Const value and pattern"),
    expect(SOME [], fn() => match(Const 10, ConstP 10), "Should return SOME [] for same Const value and pattern")
  ]);

run_tests("first_match() test suite", [
    expect(NONE, fn() => first_match (Const 16) [UnitP, ConstructorP("abc", Wildcard)], "Should return NONE"),
    expect(SOME[("b",Unit),("a",Const 1)], 
            fn() => first_match (Tuple [Unit, Const 1, Constructor("abc", Unit)]) [UnitP, ConstP 1, TupleP [UnitP, Variable "a", ConstructorP("abc", Variable "b")]],
            "Should return the matching value pattern pairs only"),
    expect(SOME [], fn () => first_match (Const 16) [UnitP, Wildcard, Variable "my_var"], 
            "Should return SOME [] for a matching pattern that does not generate binding"),
    expect(SOME [("abc", Const 15)], fn () => first_match (Const 15) [UnitP, Variable "abc", Wildcard], "Should return the name value binding"),
    expect(SOME [("abc", Const 15)], 
            fn () => first_match (Constructor("c", Const 15)) [UnitP, ConstP 32, ConstructorP("c", Variable "abc")], 
            "Should return the name value binding for constructor data type")
  ]);
