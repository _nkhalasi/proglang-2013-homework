(* Coursera Programming Languages, Homework 3 *)

exception NoAnswer

(**** solution code below ****)
(**** Problems 1 to 8 ****)
fun is_capitalized s = Char.isUpper(String.sub(s, 0))
fun only_capitals strings = List.filter is_capitalized strings 
fun longest_string1 strings = List.foldl (fn (elem, acc) => if String.size elem > String.size acc then elem else acc) "" strings
fun longest_string2 strings = List.foldl (fn (elem, acc) => if String.size elem >= String.size acc then elem else acc) "" strings

fun longest_string_helper f strings = 
  let
    fun helper(acc, strings) =
      case strings of
           [] => acc
         | elem::rest => if f(String.size elem, String.size acc)
                          then helper(elem, rest)
                          else helper(acc, rest)
  in
    helper("", strings)
  end

val longest_string3 = longest_string_helper (fn(x, y) => x > y)
val longest_string4 = longest_string_helper (fn(x, y) => x >= y)

fun longest_capitalized strings = (longest_string3 o only_capitals) strings
fun rev_string string_to_reverse = (String.implode o rev o String.explode) string_to_reverse

fun first_answer answer_finder answers =
  case answers of
       [] => raise NoAnswer
     | answer::other_answers => case answer_finder answer of
                                     NONE => first_answer answer_finder other_answers
                                   | SOME ans => ans

fun all_answers answer_finder answers = 
  let
    fun collate_answers acc answers =
      case (acc, answers) of
           (NONE, _) => NONE
         | (SOME x, []) => SOME x
         | (SOME x, ans::anss) =>
             case answer_finder ans of
                  NONE => collate_answers NONE anss
                | SOME lst => collate_answers (SOME (lst @ x)) anss
  in
    collate_answers (SOME []) answers
  end

(**** Problems 9 to 12 ****)
datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** Solution code here ****)
val count_wildcards = g (fn() => 1) (fn s => 0)
val count_wild_and_variable_lengths = g (fn() => 1) (fn s => String.size s)
fun count_some_var(varname, pat) = g (fn() => 0) (fn s => if s = varname then 1 else 0) pat

fun has_repeats xs = 
  case xs of
       [] => false
     | x1::xs' => List.exists (fn x => x = x1) xs' orelse has_repeats xs'

fun check_pat pat = 
  let
    fun collect_variable_names(ps, names) = 
      List.foldl (fn(pat, names) => case pat of 
                                         Variable varname => varname :: names 
                                       | TupleP plist => collect_variable_names(plist, names) 
                                       | _ => names) names ps
    val ps = case pat of Variable pname => [pat] | TupleP ps' => ps' | _ => []
  in
    if has_repeats (collect_variable_names(ps, [])) then false else true
  end

fun match (vl, pt) = 
  case (vl, pt) of
       (Unit, UnitP) => SOME []
     | (Const x, ConstP y) => if x = y then SOME [] else NONE
     | (_, Wildcard) => SOME []
     | (v, Variable s) => SOME [(s, v)]
     | (Tuple vlist, TupleP plist) => 
         if (List.length vlist) = (List.length plist) 
         then all_answers (fn (v, p) => match(v, p)) (ListPair.zip (vlist, plist)) 
         else NONE
     | (Constructor(s1,v), ConstructorP(s2,p)) => if s1 = s2 then match(v, p) else SOME []
     | _ => NONE

fun first_match vl pt_list = 
  SOME(first_answer (fn pt => match(vl, pt)) pt_list) handle NoAnswer => NONE

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string

(* I have an implementation but I don't think I have understood the requirements
* very well. especially about how to use type def list (string * string * typ)
* list in the solution. Hence holding off from putting any further efforts. *)
(*
fun typecheck_patterns(typ_def_list, pat_list) =
  let
    fun does_typ_check_pattern(atyp, apat) =
      case (atyp, apat) of
           (UnitT, UnitP) => true
         | (IntT, ConstP _) => true
         | (TupleT tlist, TupleP plist) => 
             if (List.length tlist = List.length plist) 
             then List.all (fn pat_chk_res => pat_chk_res) (List.map (fn (t,p) => does_typ_check_pattern(t, p)) (ListPair.zip (tlist, plist)))
             else false
         | (Datatype _, Variable _) => true
         | (Datatype _, ConstructorP(_, _)) => true
         | (Anything, Wildcard) => true
         | (Anything, UnitP) => true
         | (Anything, ConstP _) => true
         | (Anything, TupleP _) => true
         | (Anything, Variable _) => true
         | (Anything, ConstructorP(_, _)) => true
         | _ => false

    fun does_typ_check_patterns(atyp) =
      List.all (fn pat_check_result => pat_check_result) (List.map (fn pat => does_typ_check_pattern(atyp, pat)) pat_list)
  in
    case typ_def_list of
         [] => NONE
       | (cname, dname, atyp)::other_typ_defs => 
           if does_typ_check_patterns(atyp) 
           then SOME atyp
           else typecheck_patterns(other_typ_defs, pat_list)
  end
*)
