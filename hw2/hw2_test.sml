use "hw2.sml";

fun println() =
  print("\n")

fun expect(expected, func, message) =
  if func() = expected
  then print("Ok..")
  else print("Failed: " ^ message ^ "..")

fun run_test_suite(name: string, test_suite_func) =
  let
  in
    println();
    print("Running " ^ name ^ "\n");
    test_suite_func();
    println()
  end

fun all_except_option_test_suite() =
  let
  in
    expect(NONE, fn() => (all_except_option("abc", [])), "Should return NONE for an empty list");
    expect(NONE, fn() => (all_except_option("abc", ["Naresh"])), 
          "Should return NONE when name is not found in the list - case 1");
    expect(NONE, fn() => (all_except_option("abc", ["Naresh", "Robert"])), 
          "Should return NONE when name is not found in the list - case 2");
    expect(NONE, fn() => (all_except_option("abc", ["Naresh", "Robert", "Krishang"])), 
          "Should return NONE when name is not found in the list - case 3");
    expect(SOME ["Robert"], fn() => (all_except_option("Naresh", ["Naresh", "Robert"])), 
          "Should return the list with the name excluded");
    expect(SOME ["Naresh", "Krishang"], fn() => (all_except_option("Robert", ["Naresh", "Robert", "Krishang"])), 
          "Should return the list with the name excluded")
  end;

fun get_substitutions_test_suite(func) = 
  let
  in
    expect([], fn() => (func([], "abc")), "Should return an empty list for an empty list");
    expect([], fn() => (func([["Naresh"]], "abc")),
                  "Should return an empty list when given name not found in the single list");
    expect([], fn() => (func([["Naresh"], ["Robert"]], "abc")),
                  "Should return an empty list when given name not found in any list - case 1");
    expect([], fn() => (func([["Naresh", "Rajesh"], ["Robert", "Thomas"]], "abc")),
                  "Should return an empty list when given name not found in any list - case 2");
    expect(["Fredrick", "Freddie", "F"], fn() => (func(
                    [["Fred", "Fredrick"], ["Elizabeth", "Betty"], ["Freddie", "Fred", "F"]],
                    "Fred"
                  )),
                  "Should return a combined list from the lists where the name was found");
    expect(["Jeffrey", "Geoff", "Jeffrey"], fn() => (func(
                    [["Fred", "Fredrick"], ["Jeff", "Jeffrey"], ["Geoff", "Jeff", "Jeffrey"]],
                    "Jeff"
                  )),
                  "Should return a combined list from the lists where the name was found - case 2")
  end;

fun similar_names_test_suite() =
  let
  in
    expect([{first="Naresh", last="Khalasi", middle="J"}], 
            fn() => (similar_names([],
                                    {first="Naresh", last="Khalasi", middle="J"})), 
                                    "Should return a list with input name for an empty list");
    expect([{first="Naresh", last="Khalasi", middle="J"}], 
            fn() => (similar_names([["Fred", "Fredrick"], ["Robert", "Thomas"]],
                                    {first="Naresh", last="Khalasi", middle="J"})), 
                                    "Should return a list with input name when name not found in any list");
    expect([ {first="Fred", last="Smith", middle="W"}, {first="Fredrick", last="Smith", middle="W"}, {first="Freddie", last="Smith", middle="W"}, {first="F", last="Smith", middle="W"} ], 
            fn() => (similar_names([["Fred", "Fredrick"], ["Naresh", "Krishang"], ["Freddie", "Fred", "F"]],
                                    {first="Fred", last="Smith", middle="W"})), 
                                    "Should return all possible names")
  end;

run_test_suite("all_except_option_test_suite()", fn() => (all_except_option_test_suite()));
run_test_suite("get_substitutions1_test_suite()", fn() => (get_substitutions_test_suite(get_substitutions1)));
run_test_suite("get_substitutions2_test_suite()", fn() => (get_substitutions_test_suite(get_substitutions2)));
run_test_suite("similar_names_test_suite()", fn() => (similar_names_test_suite()));

fun card_color_test_suite() = 
let
in
  expect(Black, fn() => (card_color(Clubs, Ace)), "Should return Black for Ace of Clubs");
  expect(Black, fn() => (card_color(Clubs, Num 2)), "Should return Black for Two of Clubs");
  expect(Black, fn() => (card_color(Spades, Ace)), "Should return Black for Ace of Spades");
  expect(Black, fn() => (card_color(Spades, Num 2)), "Should return Black for Two of Spades");
  expect(Red, fn() => (card_color(Hearts, King)), "Should return Red for King of Hearts");
  expect(Red, fn() => (card_color(Hearts, Num 2)), "Should return Red for Two of Hearts");
  expect(Red, fn() => (card_color(Diamonds, King)), "Should return Red for King of Diamonds");
  expect(Red, fn() => (card_color(Diamonds, Num 2)), "Should return Red for Two of Diamonds")
end;

fun card_value_test_suite() = 
let
in
  expect(11, fn() => (card_value(Clubs, Ace)), "Should return 11 for Ace of Clubs");
  expect(2, fn() => (card_value(Clubs, Num 2)), "Should return 2 for Two of Clubs");
  expect(10, fn() => (card_value(Spades, Jack)), "Should return 10 for Jack of Spades");
  expect(8, fn() => (card_value(Spades, Num 8)), "Should return 8 for Eight of Spades");
  expect(10, fn() => (card_value(Hearts, King)), "Should return 10 for King of Hearts");
  expect(10, fn() => (card_value(Diamonds, Queen)), "Should return 10 for Queen of Diamonds")
end;

exception CardNotFound

fun remove_card_test_suite() = 
  let
  in
    expect([(Clubs, Num 99)], 
          fn() => (remove_card([], (Clubs, Ace), CardNotFound) handle CardNotFound => [(Clubs, Num 99)]), 
          "Should return [(Clubs, 99)] for an empty list");
    expect([(Clubs, Num 99)], 
          fn() => (remove_card([(Clubs, Num 2), (Clubs, King)], (Clubs, Ace), CardNotFound) handle CardNotFound => [(Clubs, Num 99)]), 
          "Should return [(Clubs, 99)] when card not found in the list");
    expect([(Clubs, King), (Clubs, Num 2), (Spades, Ace)], 
          fn() => (remove_card([(Clubs, Num 2), (Clubs, King), (Clubs, Num 2), (Spades, Ace)], 
                              (Clubs, Num 2), CardNotFound) 
                      handle CardNotFound => [(Clubs, Num 99)]), 
          "Should return [(Clubs, King), (Clubs, Num 2), (Spades, Ace)]");
    expect([(Clubs, King), (Spades, Ace)], 
          fn() => (remove_card([(Clubs, King), (Clubs, Num 2), (Spades, Ace)], 
                              (Clubs, Num 2), CardNotFound) 
                      handle CardNotFound => [(Clubs, Num 99)]), 
          "Should return [(Clubs, King), (Spades, Ace)]")
  end;

fun all_same_color_test_suite() = 
  let
  in
    expect(true, fn() => (all_same_color([])), "Should return true for an empty list");
    expect(true, fn() => (all_same_color([(Clubs, Ace)])), "Should return true for a single element list");
    expect(true, fn() => (all_same_color([(Clubs, Ace), (Clubs, King), (Spades, Num 10)])), 
          "Should return true for list containing clubs and spades");
    expect(true, fn() => (all_same_color([(Hearts, Ace), (Diamonds, King)])), 
          "Should return true for list containing hearts and diamonds");
    expect(false, fn() => (all_same_color([(Clubs, Ace), (Diamonds, King)])), 
          "Should return true for list containing clubs and diamonds")
  end;

fun sum_cards_test_suite() = 
  let
  in
    expect(0, fn() => (sum_cards([])), "Should return 0 for an empty cards list");
    expect(10, fn() => (sum_cards([(Clubs, Num 10)])), "Should return 10 for ten of clubs");
    expect(21, fn() => (sum_cards([(Clubs, Num 10), (Clubs, Ace)])), 
          "Should return 21 for ten and ace of clubs");
    expect(31, fn() => (sum_cards([(Spades, Ace), (Spades, Num 2), (Spades, Num 3), (Diamonds, Num 4), 
                                    (Clubs, Num 5), (Hearts, Num 6)])), 
          "Should return 31 for Ace to Six")
  end;

fun score_test_suite() = 
  let
  in
    expect(3, fn() => (score([(Hearts, Num 2), (Diamonds, Num 2)], 10)), "Should return 3");
    expect(4, fn() => (score([(Hearts, Num 2), (Diamonds, Num 2)], 1)), "Should return 4");
    expect(6, fn() => (score([(Clubs, Num 2), (Diamonds, Num 2)], 10)), "Should return 6");
    expect(9, fn() => (score([(Clubs, Num 2), (Diamonds, Num 2)], 1)), "Should return 9")
  end;

fun officiate_test_suite() =
  let
  in
    expect(~9999, 
           fn() => (officiate([(Clubs, Jack), (Spades, Num(8))],
                     [Draw, Discard(Hearts, Jack)], 42) handle IllegalMove => ~9999),
          "Should raise IllegalMove exception");
    expect(3, fn() => (officiate([(Clubs, Ace), (Spades, Ace), (Clubs, Ace), (Spades, Ace)],
                        [Draw, Draw, Draw, Draw, Draw], 42)),
            "Should return a score of 3");
    expect(19, fn() => (officiate([(Hearts, Jack), (Diamonds, King), (Clubs, Num(8)), 
                                  (Hearts, Num(2)), (Diamonds, Num(9)), (Spades, Num(4)), 
                                  (Clubs, Num(5)), (Spades, Num(10)), (Hearts, Num(3))],
                        [Draw, Draw, Draw, Draw, Discard(Diamonds, King), Draw, Discard(Hearts, Jack), Draw], 
                        42)),
            "Should return a score of 19");
    expect(23, fn() => (officiate([(Hearts, Jack), (Diamonds, King), (Clubs, Num(8)), 
                                  (Hearts, Num(2)), (Diamonds, Num(9)), (Spades, Num(4)), 
                                  (Clubs, Num(5)), (Spades, Num(10)), (Hearts, Num(3))],
                        [Draw, Draw, Draw, Draw, Discard(Diamonds, King), Draw, 
                         Discard(Hearts, Jack), Draw, Discard(Diamonds, Num(9)), Draw], 
                        42)),
            "Should return a score of 23");
    expect(6, fn() => (officiate([(Hearts, Jack), (Diamonds, King), (Clubs, Num(8)), 
                                  (Spades, Num(2)), (Diamonds, Num(9)), (Spades, Num(4)), 
                                  (Clubs, Num(5)), (Spades, Num(10)), (Hearts, Num(3)), 
                                  (Spades, Num(3))],
                        [Draw, Draw, Draw, Draw, Discard(Diamonds, King), Draw, 
                         Discard(Hearts, Jack), Draw, Discard(Diamonds, Num(9)), 
                         Draw, Draw, Draw, Discard(Hearts, Num(3))], 
                        42)),
            "Should return a score of 6");
    expect(9, fn() => (officiate([(Hearts, Jack), (Diamonds, King), (Clubs, Num(8)), 
                                  (Spades, Num(2)), (Diamonds, Num(9)), (Spades, Num(4)), 
                                  (Clubs, Num(5)), (Spades, Ace), (Hearts, Num(3)), 
                                  (Spades, Num(3))],
                        [Draw, Draw, Draw, Draw, Discard(Diamonds, King), Draw, 
                         Discard(Hearts, Jack), Draw, Discard(Diamonds, Num(9)), 
                         Draw, Draw, Draw, Discard(Hearts, Num(3))], 
                        30)),
            "Should return a score of 9");
    expect(1, fn() => (officiate([(Hearts, Jack), (Diamonds, King), (Clubs, Num(8)), 
                                  (Spades, Num(2)), (Diamonds, Num(9)), (Spades, Num(4)), 
                                  (Clubs, Num(6)), (Spades, Ace), (Hearts, Num(3)), 
                                  (Spades, Num(3))],
                        [Draw, Draw, Draw, Draw, Discard(Diamonds, King), Draw, 
                         Discard(Hearts, Jack), Draw, Discard(Diamonds, Num(9)), 
                         Draw, Draw, Draw, Discard(Hearts, Num(3))], 
                        30)),
            "Should return a score of 1")
  end;

fun officiate_challenge_test_suite() =
  let
  in
    expect(1,  fn() => (officiate_challenge([(Spades, Ace)], [Draw], 10)), "Should return 1")
  end;

fun careful_player_test_suite() =
  let
  in
    expect([Draw, Draw, Discard(Spades, Num 6), Draw],
           fn() => (careful_player([(Clubs, Num 4), (Spades, Num 6), (Hearts, Ace), (Diamonds, Num 2)], 15)),
           "Case 1");
    expect([Draw, Draw], fn() => (careful_player([(Clubs, Ace), (Spades, Jack), (Hearts, Num 4)], 22)), "Case 2");
    expect([Draw, Discard(Clubs, Ace), Draw, Draw], fn() => (careful_player([(Clubs, Ace), (Spades, Jack), (Hearts, Num 4)], 21)), "Case 3");
    expect([Draw, Draw, Discard(Hearts, King), Draw], fn() => (careful_player([(Spades,Num 7),(Hearts,King),(Clubs,Ace),(Diamonds,Num 2)], 18)), "Case 4")
  end;

run_test_suite("card_color_test_suite()", fn() => (card_color_test_suite()));
run_test_suite("card_value_test_suite()", fn() => (card_value_test_suite()));
run_test_suite("remove_card_test_suite()", fn() => (remove_card_test_suite()));
run_test_suite("all_same_color_test_suite()", fn() => (all_same_color_test_suite()));
run_test_suite("sum_cards_test_suite()", fn() => (sum_cards_test_suite()));
run_test_suite("score_test_suite()", fn() => (score_test_suite()));
run_test_suite("officiate_test_suite()", fn() => (officiate_test_suite()));
run_test_suite("officiate_challenge_test_suite()", fn() => (officiate_challenge_test_suite()));
run_test_suite("careful_player_test_suite()", fn() => (careful_player_test_suite()));
