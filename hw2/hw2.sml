(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)
fun all_except_option(name, names) = 
  let 
    fun check_and_exclude(names, result) =
      case names of
           [] => NONE
         | aName::otherNames => 
             if same_string(aName, name) 
             then SOME (rev result @ otherNames)
             else check_and_exclude(otherNames, aName::result)
  in
    check_and_exclude(names, [])
  end

fun get_substitutions1(list_of_list_of_names, name) = 
    case list_of_list_of_names of
         [] => [] 
       | hd::tl => (case all_except_option(name, hd) of
                         NONE => get_substitutions1(tl, name) @ []
                       | SOME substitutions => substitutions @ get_substitutions1(tl, name))

fun get_substitutions2(list_of_list_of_names, name) = 
  let
    fun identify_substitution_lists(list_of_list_of_names, result) =
      case list_of_list_of_names of
           [] => result 
         | hd::tl => (case all_except_option(name, hd) of
                           NONE => identify_substitution_lists(tl, result)
                         | SOME substitutions => identify_substitution_lists(tl, result @ substitutions))
  in
    identify_substitution_lists(list_of_list_of_names, [])
  end

fun similar_names(list_of_list_of_names, name_record) = 
  let
    val {first=fname, last=lname, middle=mname} = name_record
    fun build_similar_names(list_of_names, result) = 
      case list_of_names of
           [] => rev result
         | hd::tl => build_similar_names(tl, {first=hd, last=(lname), middle=(mname)} :: result)
  in
    build_similar_names(get_substitutions2(list_of_list_of_names, fname), [name_record])
  end

fun similar_names_1(list_of_list_of_names, name_record) = 
  let
    val {first=fname, last=lname, middle=mname} = name_record
    fun build_similar_names(list_of_names) = 
      case list_of_names of
           [] => []
         | hd::tl => {first=hd, last=(lname), middle=(mname)} :: build_similar_names(tl)
  in
    name_record :: build_similar_names(get_substitutions2(list_of_list_of_names, fname))
  end

(* you may assume that Num is always used with values 2, 3, ..., 9
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color(card_instance) = 
  case card_instance of
       (Clubs, _) => Black
     | (Spades, _) => Black
     | _ => Red

fun card_value card_instance = 
  case card_instance of
       (_, Ace) => 11
     | (_, Jack) => 10
     | (_, Queen) => 10
     | (_, King) => 10
     | (_, Num id) => id

fun remove_card(cs, c, e) = 
  let
    fun check_and_exclude(cards, result) = 
      case cards of
           [] => raise e
         | aCard::otherCards => 
             if aCard = c
             then rev result @ otherCards
             else check_and_exclude(otherCards, aCard::result)
  in
    case cs of
         [] => raise e
       | _ => check_and_exclude(cs, [])
  end

fun all_same_color list_of_cards = 
  case list_of_cards of
       [] => true
     | first::[] => true
     | first::(second::rest) => (card_color(first) = card_color(second) andalso all_same_color (second::rest))

fun sum_cards list_of_cards = 
  let
    fun value_of_cards(cards, value) = 
      case cards of
           [] => value
         | c::others => value_of_cards(others, value + card_value(c))
  in
    value_of_cards(list_of_cards, 0)
  end

fun score(held_cards, goal) = 
  let
    val sum = sum_cards held_cards
    val preliminary_score = if sum > goal then 3 * (sum - goal) else goal - sum
  in
    case all_same_color held_cards of
         true => preliminary_score div 2
       | false => preliminary_score
  end

fun officiate(cards, moves, goal) = 
  let
    fun play_game(cards, moves, held_cards) =
      case (cards, moves) of
           ([], []) => score(held_cards, goal)
         | ([], _) =>  score(held_cards, goal)
         | (_, []) =>  score(held_cards, goal)
         | (aCard::otherCards, aMove::otherMoves) => 
             (case aMove of
                   Draw => let 
                             val new_held_cards = aCard::held_cards
                           in
                             if sum_cards(new_held_cards) > goal
                             then score(new_held_cards, goal)
                             else play_game(otherCards, otherMoves, new_held_cards)
                           end
                 | Discard aCard => play_game(cards, otherMoves, remove_card(held_cards, aCard, IllegalMove)))
  in
    play_game(cards, moves, [])
  end

(* Challenge Problems *)
(* helper function to calculate score when Ace=1 *)
fun count_aces cards =
  case cards of
       [] => 0
     | (_, Ace)::rest => 1 + count_aces(rest)
     | _::rest => count_aces(rest)

fun score_challenge(held_cards, goal) =
  let
    val sum1 = sum_cards held_cards
    val sum2 = sum1 - (count_aces(held_cards) * 10)
    val pscore1 = if sum1 > goal then 3 * (sum1 - goal) else goal - sum1
    val pscore2 = if sum2 > goal then 3 * (sum2 - goal) else goal - sum2
    val preliminary_score = if pscore1 < pscore2 then pscore1 else pscore2
  in
    case all_same_color held_cards of
         true => preliminary_score div 2
       | false => preliminary_score
  end

(* ideally one would want to pass the score or score_challenge function and
* reuse the logic in officiate *)
fun officiate_challenge(cards, moves, goal) = 
  let
    fun play_game(cards, moves, held_cards) =
      case (cards, moves) of
           ([], []) => score_challenge(held_cards, goal)
         | ([], _) =>  score_challenge(held_cards, goal)
         | (_, []) =>  score_challenge(held_cards, goal)
         | (aCard::otherCards, aMove::otherMoves) => 
             (case aMove of
                   Draw => let 
                             val new_held_cards = aCard::held_cards
                           in
                             if sum_cards(new_held_cards) > goal
                             then score_challenge(new_held_cards, goal)
                             else play_game(otherCards, otherMoves, new_held_cards)
                           end
                 | Discard aCard => play_game(cards, otherMoves, remove_card(held_cards, aCard, IllegalMove)))
  in
    play_game(cards, moves, [])
  end

(* Fails somewhere.....here is the feedback from autograder....
Your function returns an incorrect result when given a hand of
[(Spades,Num 7),(Hearts,King),(Clubs,Ace),(Diamonds,Num 2)]
and a goal of 17 [incorrect answer]
*)
fun careful_player(cards, goal) =
  let
    fun predict_moves(cards, held_cards, moves) =
      case (cards, held_cards) of
           ([], _) => rev moves
         | (first_card::rest_cards, []) => predict_moves(rest_cards, first_card::held_cards, Draw::moves)
         | (first_card::rest_cards, first_held_card::rest_held_cards) =>
             let
               val sum = sum_cards held_cards
             in
               if sum > goal orelse score(held_cards, goal) = 0 
               then rev moves
               else if score(first_card::rest_held_cards, goal) = 0
               then predict_moves(rest_cards, first_card::rest_held_cards, Draw::Discard(first_held_card)::moves)
               else if goal - sum > 10
               then predict_moves(rest_cards, first_card::held_cards, Draw::moves)
               else predict_moves(cards, rest_held_cards, Discard(first_held_card) :: moves)
             end
  in
    predict_moves(cards, [], [])
  end
